# How to use this POC

---

## Test 1: 

### Test the kakfa restart/recreat

User the first docker-compose, `docker-compose -f docker-compose_1.yml up -d`

1. all service start up. ---ALL OK
1. `restart` kakfa container ----ALL OK
1. `rm` kakfa container, then "up -d" recreate it. (the new kafka broker id is 1002) --- NOK
1. `rm` kakfa container, wait 1m , then "up -d" recreate it.the new kafka broker id is 1003) --- NOK
1. cleanup  `docker-compose -f docker-compose_1.yml down`

>
**Summary:** once the kafka recreated, the broker id will change. but the topic info keep in the zookeeper side, still using the old brocker id 1001.

### Test the zookeeper restart/recreat

User the first docker-compose, `docker-compose -f docker-compose_1.yml up -d`

1. all service start up. ---ALL OK
1. `restart` zookeeper container ----ALL OK
1. `stop` zookeeper container
    1. kafka will keep throwing error, **but it will running**
    1. service `app` still can produce msg. and still can consume msg
    1. `start` zookeeper again.  ---ALL OK
1. `stop` zookeeper, and `restart app` still can produce msg. and still can consume msg---ALL OK
1. `stop` zookeeper, and `recreat app` still can produce msg. and still can consume msg---ALL OK, it seems like all good, even without zookeeper.
1. question: we start app first. it create the topic already. enven we restart it. it still works. what if we need to create a new topic?
    1. now we `start zookeeper, kafka `first. then `stop the zookeeper`, kafka still runing ,but throwing errors. then `start app`: the app can't create topic anymore. which means the topic creation process need the zookeeper.
1. `recreat` zookeeper
    1. if the new zookeeper use the same ip, kafka is able to connect. but all the topic info is lost.  
    1. if the new zookeeper use the diffrent ip. kafka will still throw error mes attempting reconnect. (this is being fixed by the kafka new version. )
    1. once the zookeeper recreate, all the topic info is lost. but for the still runing app. producer and consumer is ok
---

## Test 1_1: 

here we fix the `kafka_brocker_id` 

User the first docker-compose, `docker-compose -f docker-compose_1_1.yml up -d`

1. all service start up. ---ALL OK
1. `restart` kakfa container ----ALL OK
1. `rm` kakfa container, then "up -d" recreate it. (the new kafka broker id is still 1) --- OK
1. since the kakfa-brocker id can be fixed to 1. what if we have multiple brocker with the same id?
    1. all service start up
    1. scale up the kafka to 2
    1. `A broker is already registered on the path /brokers/ids/1. This probably indicates that you either have configured a brokerid that is already in use`

> so now we can summarize that: 
>1. `set kafka_brocker_id for kafka, so that even the kafka recreat, it will stil keep the same node_id`

---

## Test 1_2: 

here we will try to test can we receive the msg during the time our service restart 

User the first docker-compose, `docker-compose -f docker-compose_1_2.yml up -d`

1. all service start up. ---ALL OK
1. send msg manualy `kafka-console-producer.sh --broker-list localhost:9092 --topic topic-test`  
1. stop the container
    1. `stop the app container` 
    1.  send some msg to `topic-test` 
    1. `start the app container` we can't receive the log during the time app stopped. but we can receive new msg.  -- NOT OK
1. now update the docker-compose file set the `- spring_cloud_stream_bindings_input_group=app`
    1. repeat the above steps
    1. we are able to reveive the msgs when we are down.
1. we keep the same group so that we can retrieve the unconsumed msg. what if the kafka down ? what if the kakfa be recreated?
    1. trial with stop the kafka
        1. all service start up.
        1. stop the app constainer first, 
        1. send some msg to kafka topic `kafka-console-producer.sh --broker-list localhost:9092 --topic topic-test` ,then stop the kafka container
        1. start the app and kafka container. once app startup, we still can reveive the msg
    1. trial with recreate the kafka
        1. all service start up.
        1. stop the app constainer first, 
        1. send some msg to kafka topic `kafka-console-producer.sh --broker-list localhost:9092 --topic topic-test` ,then rm the kafka container
        2. start the app and recreate kafka container. once app startup, we **can't** reveive the msg. 
    1. trial with recreate the kafka but with persist volume
        1. all service start up.
        1. stop the app constainer first, 
        1. send some msg to kafka topic `kafka-console-producer.sh --broker-list localhost:9092 --topic topic-test` ,then rm the kafka container
        1. start the app and recreate kafka container. once app startup, we **can't** reveive the msg.  
        1. under the /kakfa folder. kafka create *kafka-logs-035d57cd249f  kafka-logs-7334fff65b12* folder, it seems like it is using the container id
    1. trial with recreate the kafka but with persist volume with `hostname:kafka` on the kafka service
        1. under the /kakfa folder. kafka create *kafka-logs-kafka* folder
        1. stop the app constainer first, 
        1. send some msg to kafka topic `kafka-console-producer.sh --broker-list localhost:9092 --topic topic-test` ,then rm the kafka container
        1. start the app and recreate kafka container. once app startup, we **CAN** reveive the msg.  

> so now we can summarize that if we want relieable msg, we need 

>1. `set the consumer group in the app side so that even the app restart or recreate we still can retrieve the cached msg`
>1. `set the volume for kafka and set the hostname for kafka so that even the kafka recreate we still keep the msg`

---

## Test 2: 

here we will try to setup the zookeeper cluster (zk1, zk2, zk3)

User the first docker-compose, `docker-compose -f docker-compose_2.yml up -d`

1. all service start up. ---ALL OK
2. use `docker exec -it <container_id> zkServer.sh status` to check which is the leader (zk3)
3. stop zk3:
	1. ALL OK
	2. check the leader again , now zk2 is the leader
4. DO NOT TRY TO STOP ANY MORE ZK Container.


---

## Test 3: 

here we will try to setup the zookeeper cluster (zk1, zk2, zk3) + kafka cluster setup (kafka1 + kafka2)

> Comming Soon




