package com.bosch.procon.consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.GenericMessage;

/**
 * created by Bai Liming(liming.bai@sg.bosch.com) at 2019-10-12
 */

@EnableBinding(value = {
    TestConsumer.TestSource.class
})
public class TestConsumer {
    private final Logger log = LoggerFactory.getLogger(TestConsumer.class);

    @StreamListener(TestSource.INPUT)
    public void process(GenericMessage<String> message) {
        log.info(message.getPayload());
    }
    public interface TestSource {
        String INPUT = "input";
        @Input(INPUT) MessageChannel output();
    }
}
